-keep class com.wilshireaxon.android.core.app.CoreApplication {
    <init>();
    void attachBaseContext(android.content.Context);
}
-keep class com.urbanairship.actions.ActionActivity { <init>(); }
-keep class com.urbanairship.CoreActivity { <init>(); }
-keep class com.urbanairship.actions.LandingPageActivity { <init>(); }
-keep class com.wilshireaxon.android.core.activity.winterwonderland.DetailActivity { <init>(); }
-keep class com.wilshireaxon.android.core.activity.StartupActivity { <init>(); }
-keep class com.wilshireaxon.android.core.activity.SignInLandingActivity { <init>(); }
-keep class com.wilshireaxon.android.core.activity.AttractionActivity { <init>(); }
-keep class com.wilshireaxon.android.core.activity.sports.GenericFeed1Activity { <init>(); }
-keep class com.wilshireaxon.android.core.activity.GenericListActivity { <init>(); }
-keep class com.wilshireaxon.android.core.activity.CreateAccountActivity { <init>(); }
-keep class com.wilshireaxon.android.core.activity.ForgotPasswordActivity { <init>(); }
-keep class com.wilshireaxon.android.core.activity.LoginActivity { <init>(); }
-keep class com.wilshireaxon.android.core.activity.RewardsTabStripActivity { <init>(); }
-keep class com.wilshireaxon.android.core.activity.RewardRedeemActivity { <init>(); }
-keep class com.wilshireaxon.android.core.activity.MainActivity { <init>(); }
-keep class com.wilshireaxon.android.core.activity.WebViewActivity { <init>(); }
-keep class com.wilshireaxon.android.core.activity.SettingsActivity { <init>(); }
-keep class com.wilshireaxon.android.core.activity.MyProfileActivity { <init>(); }
-keep class com.wilshireaxon.android.core.development.DevelopmentActivity { <init>(); }
-keep class com.wilshireaxon.android.core.activity.GalleryActivity { <init>(); }
-keep class com.wilshireaxon.android.core.activity.QRScannerActivity { <init>(); }
-keep class com.urbanairship.push.GCMPushReceiver { <init>(); }
-keep class com.urbanairship.CoreReceiver { <init>(); }
-keep class com.wilshireaxon.android.core.receivers.PushNotificationReceiver { <init>(); }
-keep class com.urbanairship.push.PushService { <init>(); }
-keep class com.urbanairship.actions.ActionService { <init>(); }
-keep class com.urbanairship.richpush.RichPushUpdateService { <init>(); }
-keep class com.urbanairship.location.LocationService { <init>(); }
-keep class com.wilshireaxon.android.core.provider.TeamAppProvider { <init>(); }
-keep class com.wilshireaxon.android.core.provider.ListProvider { <init>(); }
-keep class com.wilshireaxon.android.core.provider.StatsProvider { <init>(); }
-keep class com.tyczj.extendedcalendarview.CalendarProvider { <init>(); }
-keep class com.urbanairship.UrbanAirshipProvider { <init>(); }
-keep class com.google.android.gms.ads.AdActivity { <init>(); }
-keep class com.google.android.gms.ads.purchase.InAppPurchaseActivity { <init>(); }
-keep class com.google.android.gms.auth.api.signin.internal.SignInHubActivity { <init>(); }
-keep class com.google.android.gms.measurement.AppMeasurementContentProvider { <init>(); }
-keep class com.google.android.gms.measurement.AppMeasurementReceiver { <init>(); }
-keep class com.google.android.gms.measurement.AppMeasurementService { <init>(); }
-keep public class * extends android.app.backup.BackupAgent {
    <init>();
}
-keep public class * extends java.lang.annotation.Annotation {
    *;
}
